import { StatusBar } from "expo-status-bar";
import { useState } from "react";
import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";

import {
  useFonts,
  BlackOpsOne_400Regular,
} from "@expo-google-fonts/black-ops-one";

import Storage from "./database/Storage";

export default function App() {

  const db = new Storage(); 

  const [bmiMensage, setBmiMessage] = useState("-//-");
  const [height, setHeight] = useState(1);
  const [weight, setWeight] = useState(1);

  const [color, setColor] = useState("#D4F1E3");
  const [rank, setRank] = useState("-//-");

  const calcularImc = () => {
    if (weight <= 0 || height <= 0) {
      Alert.alert("[ ERRO ] - Nenhuma medida pode ser nula");
    }
    const imc = (weight / (height * height)).toFixed(2);
    setBmiMessage(imc);

    // 18.5 - 24.9 Ok
    if (imc < 18.5) {
      setColor("#7DA0C5");
      setRank("Baixo Peso");
    } else if (imc < 24.9) {
      setColor("#77A480");
      setRank("Peso Normal");
    } else if (imc < 29.9) {
      setColor("#E2C54F");
      setRank("Excesso de Peso");
    } else if (imc < 35.0) {
      setColor("#C2741E");
      setRank("Obesidade");
    } else {
      setColor("#C32338");
      setRank("Obesidade Extrema");
    }

    const date = new Date()
    const now = date.getDate();
    
    db.add(height,weight, now);
  };

  let [fontsLoaded] = useFonts({
    BlackOpsOne_400Regular,
  });

  if (!fontsLoaded) {
    return null;
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#A10D00" translucent={false}></StatusBar>

      <View style={styles.topBar}>
        <Text style={styles.topBarText}>Sua Saúde</Text>
      </View>
      <View style={styles.areaInputs}>
        {/* ALTURA */}
        <View style={styles.containerInput}>
          <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
            <Image
              source={require("./assets/Ruler.png")}
              style={styles.image}
            ></Image>
            <Text style={styles.label}>Altura</Text>
          </View>
          <TextInput
            placeholder="1.75 m"
            keyboardType="numeric"
            style={styles.input}
            onChangeText={(value) => setHeight(value)}
          ></TextInput>
        </View>
        {/* PESO */}
        <View style={styles.containerInput}>
          <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
            <Image
              source={require("./assets/Balance.png")}
              style={styles.image}
            ></Image>
            <Text style={styles.label}>Peso</Text>
          </View>
          <TextInput
            placeholder="72.64 Kg"
            keyboardType="numeric"
            style={styles.input}
            onChangeText={(value) => setWeight(value)}
          ></TextInput>
        </View>

        <TouchableOpacity
          style={[styles.containerInput, { backgroundColor: color }]}
          onPress={calcularImc}
        >
          <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
            <Image
              source={require("./assets/bmi.png")}
              style={styles.image}
            ></Image>
            <Text style={styles.label}>Calcular IMC</Text>
          </View>
          <Text style={styles.response}>{bmiMensage} </Text>
          <Text style={styles.response}>{rank}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ECECEC",
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  topBar: {
    width: "100%",
    height: 80,

    alignItems: "center",
    justifyContent: "center",

    backgroundColor: "#ED1400",
  },
  topBarText: {
    color: "#fff",

    fontSize: 40,
    fontFamily:"BlackOpsOne_400Regular",
    textTransform: "uppercase",
  },
  areaInputs: {
    flex: 1,
    flexDirection: "column",
    alignItems: "flex-start",
    alignContent: "center",
    justifyContent: "flex-start",

    margin: 15,
  },
  containerInput: {
    alignItems: "flex-start",
    alignContent: "center",
    justifyContent: "center",

    padding: 15,

    minWidth: "100%",
    marginBottom: 15,

    borderWidth: 0.5,
    borderRadius: 15,
    borderBottomWidth: 4,
    borderColor: "#9E9E9E",
  },
  label: {
    marginLeft: 15,

    fontSize: 30,
    textTransform: "capitalize",

    color: "#454545",
  },
  input: {
    minWidth: "100%",
    marginTop: 15,

    alignItems: "center",
    justifyContent: "flex-start",
    borderRadius: 15,

    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: "#E3E3E3",

    fontSize: 16,
    textTransform: "capitalize",

    color: "#676767",
  },
  image: {
    height: 50,
    width: 50,
  },
  response: {
    minWidth: "100%",
    marginTop: 15,

    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    borderRadius: 15,

    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: "#E3E3E3",

    fontSize: 16,
    textTransform: "capitalize",

    color: "#676767",
  },
});
