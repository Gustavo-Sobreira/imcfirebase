import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyCGWA-lwEc0Lgm0R_57qsNfE4uLFDb3N2s",
    authDomain: "login-324eb.firebaseapp.com",
    projectId: "login-324eb",
    storageBucket: "login-324eb.appspot.com",
    messagingSenderId: "933798512021",
    appId: "1:933798512021:web:7f4702fb2daeea0b7fa29c",
    measurementId: "G-WHQ74MH4ET"
  };

  // Initialize Firebase
  if (!firebase.apps.length) {
     firebase.initializeApp(firebaseConfig);
  }

  export default firebase;  