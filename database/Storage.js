import AsyncStorage from '@react-native-async-storage/async-storage';

//https://reactnative.dev/docs/asyncstorage
// Erro

import firebase from './Firebase';

export default class Storage {

    listContents(onContentUpdate) {
        let contents = [];
        querySnapshot.forEach((doc)=>{
            const { name, desc } = doc.data();
            let id = doc.id;
            contents.push({id, name, desc});
        });
        onContentUpdate(contents);
    }; 

    edit(index, content) {
        firebase.firestore().collection('contents').doc(index).set(content);         
    }

    add(content) {
        firebase.firestore().collection('contents').add(content);         
    }

    deleteContent(index) {
        firebase.firestore().collection('contents').doc(index).delete().then(() => {
            console.log('Apagado com sucesso');
        })
        .catch((error) => {
            console.log(error);
        });
    }

}